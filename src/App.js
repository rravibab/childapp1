import * as React from 'react';
import "@informatica/droplets-core/dist/themes/archipelago/archipelago.css";
import { Shell } from '@informatica/droplets-common';
import { Panel,Table,Button } from '@informatica/droplets-core';
import './App.css'



class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { count:props.props.data(), data: [
      {
        name: 'abc',
        id: '123'
      },
      {
        name: 'def',
        id: '456'
      },
      {
        name: 'ghi',
        id: '789'
      }
    ] };
    this.handleClick = this.handleClick.bind(this);
  }
  
  componentWillMount() {
    // this.setState({
    //   text: "",
    // });
  }
  handleClick() {
    let updatecount=this.state.count+1
    this.setState({count: updatecount},() => {
      const event = new CustomEvent("myEvent", {detail : this.state.count})
      window.dispatchEvent(event) 
  });
     }
  render() {
    return( <div className="App">
    <Shell.Page>
    <Panel title="Micro Application --> 1">
      <Table>
        <Table.Header>
          <Table.HeaderRow>
            <Table.HeaderCell className='rowStyle'>Name</Table.HeaderCell>
            <Table.HeaderCell className='rowStyle'>Tenant Id</Table.HeaderCell>
          </Table.HeaderRow>
        </Table.Header>
        <Table.Body>
          {this.state.data.map(row => (
            <Table.Row key={row.id}>
              <Table.Cell className='rowStyleName'>{row.name}</Table.Cell>
              <Table.Cell  className='rowStyleId'>{row.id}</Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    </Panel>
    <Panel title="Inter App">
     <Button variant="primary" onClick={this.handleClick}>update Count {this.state.count}</Button>
    </Panel>
  </Shell.Page>
  </div>); 
  }
}
  
export default App;

