import React from "react";
import App from "./App";
export default function Root(props) {
  return <div><App props={props}/></div>;
}
