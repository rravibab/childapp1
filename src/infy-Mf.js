import React from "react";
import ReactDOM from "react-dom";
import singleSpaReact from "single-spa-react";
import Root from "./root.component";
function domElementGetter() {
  return document.getElementById('childApp');
}

const lifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: Root,
  domElementGetter,
});
export function mount(props) {
  // do something with the common authToken in app1
  console.log("vikram1234",props);
  return lifecycles.mount(props);
}
function bootstrap(props) {
  const {
    name, // The name of the application
    singleSpa, // The singleSpa instance
    mountParcel, // Function for manually mounting
    customProps, // Additional custom information
  } = props; // Props are given to every lifecycle
  return Promise.resolve();
}
//export const bootstrap = lifecycles.bootstrap;
//xport const mount = lifecycles.mount;
export const unmount = lifecycles.unmount;